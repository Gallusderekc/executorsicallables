package cat.itb.ex1;

import java.util.concurrent.Callable;

public class SumarFilasCallable implements Callable<Integer> {
    private int [] fila;

    public SumarFilasCallable(int[] fila) {
        this.fila = fila;
    }

    @Override
    public Integer call() throws Exception {
        int sumaFila=0;
        for(int i =0;i<fila.length;i++){
            sumaFila=sumaFila+fila[i];
        }
        return sumaFila;
    }
}
