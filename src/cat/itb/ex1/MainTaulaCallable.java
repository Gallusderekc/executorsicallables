package cat.itb.ex1;

import java.util.concurrent.*;

public class MainTaulaCallable {
    public static void main(String[] args) {
        int[][]taula = omplirTaula();

        ExecutorService ex = Executors.newFixedThreadPool(3);

        SumarFilasCallable []sumaFiles=new SumarFilasCallable[taula.length];
        Future<Integer>[] resultats = new Future[taula.length];
        try{
            int total = 0;
            for(int i =0;i<taula.length;i++){
                sumaFiles[i] = new SumarFilasCallable(taula[i]);
                resultats[i]=ex.submit(sumaFiles[i]);
                int sumaFila=resultats[i].get();
                System.out.println("la fila suma "+sumaFila);
                total = total + resultats[i].get();
            }
            ex.shutdown();
            System.out.println("la suma total es: " +total);

        }catch (InterruptedException | ExecutionException e){}
    }

    public static int[][] omplirTaula(){
        return omplirTaula();
    }
}
